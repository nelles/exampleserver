CREATE TABLE `stoneman`.`donation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `amount` FLOAT NULL,
  `nickname` VARCHAR(45) NULL,
  `state` VARCHAR(10) NULL,
  `cid` INT NULL COMMENT 'campaign id',
  PRIMARY KEY (`id`));


CREATE TABLE `stoneman`.`campaign` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(500) NULL,
  `goal` FLOAT NULL,
  `status` VARCHAR(9) NULL,
  PRIMARY KEY (`id`));
